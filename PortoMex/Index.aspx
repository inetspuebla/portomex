﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="PageIndex" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PortoMex. Puertas Autom&aacute;ticas</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- Main Theme -->
    <link rel="stylesheet" href="mainStyle.css" />
</head>
<body>
    <form id="pageForm" runat="server">
    <div id="header" >
        <div class="container">
            <div class="row">
                <div class="col-xs-2">
                    <div id="logo">
                        <h1>portomex</h1></div>
                    </div> 
                    
                <div class="col-xs-6 col-xs-offset-2">
                    <nav class="nav">
                        <ul class="nav-pills">
                            <li> <a href="#header">Inicio</a> </li>
                            <li> <a href="#home" >Nosotros</a> </li>
                            <li> <a href="#productos">Productos</a> </li>
                            <li> <a href="#servicios">Servicios</a> </li>
                            <li> <a href="#promociones">Promociones</a> </li>
                            <li> <a href="#contacto">Contacto</a> </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div id="home" >
       
    </div>
    
    <div id="nosotros">

    </div>
    
    <div id="productos">
    
    </div>

    <div id="promociones">

    </div>

    <div id="servicios">

    </div>
    
    <div id="contacto">

    </div>

    </form>
    <!-- Script Area -->
    <!-- JQuery CDN -->
    <script   src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>
</html>
